<?php

namespace App\Rules;

use App\Models\DataType;
use Illuminate\Contracts\Validation\Rule;

class ValidDataType implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validDataTypes = Datatype::all()->pluck('name', 'name');
        $allKeysFound = true;

        if (is_array($value)){
            foreach ($value as $arrValue)
            {
                if (!$validDataTypes->contains($arrValue))
                {
                    $allKeysFound = false;
                    break;
                }
            }
        }
        else{
            $allKeysFound = false;
        }

        return $allKeysFound;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You must select at least one data type and all of them most.';
    }
}
