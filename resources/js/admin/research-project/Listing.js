import AppListing from '../app-components/Listing/AppListing';

Vue.component('research-project-listing', {
    mixins: [AppListing]
});