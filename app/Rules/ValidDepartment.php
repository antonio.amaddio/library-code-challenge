<?php

namespace App\Rules;

use App\Models\DataType;
use App\Models\Department;
use Illuminate\Contracts\Validation\Rule;

class ValidDepartment implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validDepartments = Department::all()->pluck('name', 'name');
        $allKeysFound = true;

        return $validDepartments->contains($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You must select one department.';
    }
}
