<?php

namespace App\Http\Requests\Admin\ResearchProject;

use App\Rules\ValidDataType;
use App\Rules\ValidDepartment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateResearchProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.research-project.edit', $this->researchProject);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data_type' => ['required', 'array', new ValidDataType()],
            'department' => ['required', 'string', new ValidDepartment()],
            'doi' => ['nullable', 'string'],
            'project_title' => ['sometimes', 'string'],
            'responsible_researcher' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
