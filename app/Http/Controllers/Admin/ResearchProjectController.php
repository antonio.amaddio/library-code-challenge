<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ResearchProject\BulkDestroyResearchProject;
use App\Http\Requests\Admin\ResearchProject\DestroyResearchProject;
use App\Http\Requests\Admin\ResearchProject\IndexResearchProject;
use App\Http\Requests\Admin\ResearchProject\StoreResearchProject;
use App\Http\Requests\Admin\ResearchProject\UpdateResearchProject;
use App\Models\DataType;
use App\Models\ResearchProject;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ResearchProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexResearchProject $request
     * @return array|Factory|View
     */
    public function index(IndexResearchProject $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ResearchProject::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'project_title', 'responsible_researcher', 'department', 'data_type'],
            // set columns to searchIn
            ['id', 'project_title', 'responsible_researcher', 'department', 'data_type', 'doi']
        );

        foreach($data->all() as $researchProject)
        {
            $researchProject->prepareDataTypeAttributeForIndex();
        }

        if ($request->ajax()) {

            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.research-project.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.research-project.create');

        return view('admin.research-project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreResearchProject $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreResearchProject $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the ResearchProject
        $researchProject = ResearchProject::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/research-projects'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/research-projects');
    }

    /**
     * Display the specified resource.
     *
     * @param ResearchProject $researchProject
     * @throws AuthorizationException
     * @return void
     */
    public function show(ResearchProject $researchProject)
    {
        $this->authorize('admin.research-project.show', $researchProject);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ResearchProject $researchProject
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ResearchProject $researchProject)
    {
        $this->authorize('admin.research-project.edit', $researchProject);

        $researchProject->prepareDataTypeAttributeForEdit();

        return view('admin.research-project.edit', [
            'researchProject' => $researchProject,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateResearchProject $request
     * @param ResearchProject $researchProject
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateResearchProject $request, ResearchProject $researchProject)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ResearchProject
        $researchProject->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/research-projects'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/research-projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyResearchProject $request
     * @param ResearchProject $researchProject
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyResearchProject $request, ResearchProject $researchProject)
    {
        $researchProject->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyResearchProject $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyResearchProject $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ResearchProject::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
