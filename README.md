# Project Description

The following instructions were given to complete the coding challenge.

> "Please implement a dynamic web page that presents an overview of the research projects run at the Max-Planck-Institute for Human Development.

> A project should be addable through a simple survey mask. Required fields are project title, responsible researcher, department (ARC, LIP, CHM or HoE), data type (Online Survey, EEG, Behavioral, Simulation, MRI, Video, multiple options possible). An optional field might contain an identifier to a corresponding publication supported by Semantic Scholar (https://www.semanticscholar.org), e.g. DOI or ArXiv ID. A unique project ID is assigned to each project entered.  

> All projects are displayed on a webpage in a tabular form. There is a filter based on data type and department. If an identifier to a publication is provided, the title of the publication is displayed along with the current number of citations the publications received (as provided by https://www.semanticscholar.org).

> Optional: Prepare the project for deployment i.e. through dockerization.  

> User management as well as editing of projects after submission, is not required. We expect the task to be solved within 6 hours maximum. Please organize your time accordingly and cut requirements as necessary.  

> The solution can be programmed from scratch, an open-source solution can be adopted, or existing services can be composed.

> Please provide us with the source code conveniently or showcase your solution in another suitable manner. Be prepared to present it during the interview in English or in German."  

# Installation

- rename .env.example to .env file and amend configuration
- docker-compose up
- php artisan key:generate
- php artisan migrate:refresh --seed
- npm install && npn run dev

## Log in credentials

- user: doe@mpib-berlin.mpg.de
- password is: `password`

## Example showcase

![](https://amaddio.de/mpib-library-code-challenge/research-overview.gif)
