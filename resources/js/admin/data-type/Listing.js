import AppListing from '../app-components/Listing/AppListing';

Vue.component('data-type-listing', {
    mixins: [AppListing]
});