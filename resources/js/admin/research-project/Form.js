import AppForm from '../app-components/Form/AppForm';

Vue.component('research-project-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                data_type:  '' ,
                department:  '' ,
                doi:  '' ,
                project_title:  '' ,
                responsible_researcher:  '' ,
                
            }
        }
    }

});