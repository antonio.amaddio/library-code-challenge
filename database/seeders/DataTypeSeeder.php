<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_type')->insert([
                                          'name' => 'Online Survey',
                                        ]);

        DB::table('data_type')->insert([
                                          'name' => 'EEG',
                                        ]);

        DB::table('data_type')->insert([
                                          'name' => 'Behavioral',
                                        ]);

        DB::table('data_type')->insert([
                                          'name' => 'Simulation',
                                        ]);

        DB::table('data_type')->insert([
                                          'name' => 'MRI',
                                        ]);

        DB::table('data_type')->insert([
                                          'name' => 'Video',
                                        ]);
    }
}
