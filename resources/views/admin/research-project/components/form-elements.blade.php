<div class="form-group row align-items-center" :class="{'has-danger': errors.has('project_title'), 'has-success': fields.project_title && fields.project_title.valid }">
    <label for="project_title" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.research-project.columns.project_title') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.project_title" v-validate="'required'" id="project_title" name="project_title"></textarea>
        </div>
        <div v-if="errors.has('project_title')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('project_title') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('responsible_researcher'), 'has-success': fields.responsible_researcher && fields.responsible_researcher.valid }">
    <label for="responsible_researcher" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.research-project.columns.responsible_researcher') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.responsible_researcher" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('responsible_researcher'), 'form-control-success': fields.responsible_researcher && fields.responsible_researcher.valid}" id="responsible_researcher" name="responsible_researcher" placeholder="{{ trans('admin.research-project.columns.responsible_researcher') }}">
        <div v-if="errors.has('responsible_researcher')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('responsible_researcher') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('data_type'), 'has-success': fields.data_type && fields.data_type.valid }">
    <label for="data_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.research-project.columns.data_type') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect v-model="form.data_type" :options='[ "Online Survey", "EEG", "Behavioral", "Simulation", "MRI", "Video" ]' placeholder="Select data type" :multiple="true"></multiselect>

            <div v-if="errors.has('data_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('data_type') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('department'), 'has-success': fields.department && fields.department.valid }">
    <label for="department" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.research-project.columns.department') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
{{--        <input type="text" v-model="form.department" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('department'), 'form-control-success': fields.department && fields.department.valid}" id="department" name="department" placeholder="{{ trans('admin.research-project.columns.department') }}">--}}
            <multiselect v-model="form.department" :options='[ "ARC", "LIP", "CHM", "HoE" ]' placeholder="Select department" :multiple="false"></multiselect>
        <div v-if="errors.has('department')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('department') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('doi'), 'has-success': fields.doi && fields.doi.valid }">
    <label for="doi" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.research-project.columns.doi') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.doi" v-validate="''" id="doi" name="doi"></textarea>
        </div>
        <div v-if="errors.has('doi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('doi') }}</div>
    </div>
</div>


