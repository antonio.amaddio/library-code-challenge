<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_project', function (Blueprint $table) {
            $table->id();
            $table->text('project_title');
            $table->string('responsible_researcher', 255);
            $table->string('department', 255);
            $table->string('data_type', 255);
            // specification of DOI https://support.datacite.org/docs/doi-basics
            // validate doi /^10.\d{4,9}/[-._;()/:A-Z0-9]+$/i
            $table->text("doi")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_project');
    }
}
