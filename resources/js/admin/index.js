import './admin-user';
import './profile-edit-profile';
import './profile-edit-password';
import './research-project';
import './department';
import './data-type';
