<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchProject extends Model
{
    protected $table = 'research_project';

    protected $fillable = [
        'data_type',
        'department',
        'doi',
        'project_title',
        'responsible_researcher',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/research-projects/'.$this->getKey());
    }

    public function prepareDataTypeAttributeForIndex()
    {
        if (is_array($this->attributes['data_type'])){
            $this->attributes['data_type'] =  implode(', ', $this->attributes['data_type']);
        }

        $this->attributes['data_type'] = str_replace(['[', ']', '"'], '', $this->attributes['data_type']);
    }

    public function prepareDataTypeAttributeForEdit()
    {
        $string = str_replace(['[', ']', '"'], '', $this->attributes['data_type']);

        if (str_contains($string, ','))
        {
            $this->attributes['data_type'] = explode(',', $string);
        }else{
            $this->attributes['data_type'] = array($string);
        }

    }
}
