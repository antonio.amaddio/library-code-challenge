<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'research-project' => [
        'title' => 'Research Project',

        'actions' => [
            'index' => 'Research Project',
            'create' => 'New Research Project',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'project_title' => 'Project title',
            'responsible_researcher' => 'Responsible researcher',
            'department' => 'Department',
            'data_type' => 'Data type',
            'doi' => 'Digital Object Identifier',
            
        ],
    ],

    'department' => [
        'title' => 'Department',

        'actions' => [
            'index' => 'Department',
            'create' => 'New Department',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'data-type' => [
        'title' => 'Data Type',

        'actions' => [
            'index' => 'Data Type',
            'create' => 'New Data Type',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'research-project' => [
        'title' => 'Research Project',

        'actions' => [
            'index' => 'Research Project',
            'create' => 'New Research Project',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'data_type' => 'Data type',
            'department' => 'Department',
            'doi' => 'Doi',
            'project_title' => 'Project title',
            'responsible_researcher' => 'Responsible researcher',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];