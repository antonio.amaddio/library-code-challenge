<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('research-projects')->name('research-projects/')->group(static function() {
            Route::get('/',                                             'ResearchProjectController@index')->name('index');
            Route::get('/create',                                       'ResearchProjectController@create')->name('create');
            Route::post('/',                                            'ResearchProjectController@store')->name('store');
            Route::get('/{researchProject}/edit',                       'ResearchProjectController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ResearchProjectController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{researchProject}',                           'ResearchProjectController@update')->name('update');
            Route::delete('/{researchProject}',                         'ResearchProjectController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('departments')->name('departments/')->group(static function() {
            Route::get('/',                                             'DepartmentController@index')->name('index');
            Route::get('/create',                                       'DepartmentController@create')->name('create');
            Route::post('/',                                            'DepartmentController@store')->name('store');
            Route::get('/{department}/edit',                            'DepartmentController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DepartmentController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{department}',                                'DepartmentController@update')->name('update');
            Route::delete('/{department}',                              'DepartmentController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('data-types')->name('data-types/')->group(static function() {
            Route::get('/',                                             'DataTypeController@index')->name('index');
            Route::get('/create',                                       'DataTypeController@create')->name('create');
            Route::post('/',                                            'DataTypeController@store')->name('store');
            Route::get('/{dataType}/edit',                              'DataTypeController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DataTypeController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{dataType}',                                  'DataTypeController@update')->name('update');
            Route::delete('/{dataType}',                                'DataTypeController@destroy')->name('destroy');
        });
    });
});