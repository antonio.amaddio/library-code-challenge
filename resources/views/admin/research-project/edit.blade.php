@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.research-project.actions.edit', ['name' => $researchProject->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <research-project-form
                :action="'{{ $researchProject->resource_url }}'"
                :data="{{ $researchProject->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.research-project.actions.edit', ['name' => $researchProject->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.research-project.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                        <a class="btn btn-danger" :disabled="submiting" href="{{ url()->previous() }}">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.cancel') }}
                        </a>
                    </div>
                    
                </form>

        </research-project-form>

        </div>
    
</div>

@endsection