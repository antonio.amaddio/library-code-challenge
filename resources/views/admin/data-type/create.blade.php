@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.data-type.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <data-type-form
            :action="'{{ url('admin/data-types') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.data-type.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.data-type.components.form-elements')
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                    <a class="btn btn-danger" :disabled="submiting" href="{{ url()->previous() }}">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.cancel') }}
                    </a>
                </div>
                
            </form>

        </data-type-form>

        </div>

        </div>

    
@endsection