<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DataType\BulkDestroyDataType;
use App\Http\Requests\Admin\DataType\DestroyDataType;
use App\Http\Requests\Admin\DataType\IndexDataType;
use App\Http\Requests\Admin\DataType\StoreDataType;
use App\Http\Requests\Admin\DataType\UpdateDataType;
use App\Models\DataType;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DataTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexDataType $request
     * @return array|Factory|View
     */
    public function index(IndexDataType $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(DataType::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.data-type.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.data-type.create');

        return view('admin.data-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDataType $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreDataType $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the DataType
        $dataType = DataType::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/data-types'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/data-types');
    }

    /**
     * Display the specified resource.
     *
     * @param DataType $dataType
     * @throws AuthorizationException
     * @return void
     */
    public function show(DataType $dataType)
    {
        $this->authorize('admin.data-type.show', $dataType);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param DataType $dataType
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(DataType $dataType)
    {
        $this->authorize('admin.data-type.edit', $dataType);


        return view('admin.data-type.edit', [
            'dataType' => $dataType,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDataType $request
     * @param DataType $dataType
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateDataType $request, DataType $dataType)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values DataType
        $dataType->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/data-types'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/data-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyDataType $request
     * @param DataType $dataType
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyDataType $request, DataType $dataType)
    {
        $dataType->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyDataType $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyDataType $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DataType::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
