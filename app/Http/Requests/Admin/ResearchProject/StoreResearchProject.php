<?php

namespace App\Http\Requests\Admin\ResearchProject;

use App\Rules\ValidDataType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreResearchProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.research-project.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data_type' => ['required', 'array', new ValidDataType()],
            'department' => ['required', 'string'],
            'doi' => ['nullable', 'string'],
            'project_title' => ['required', 'string'],
            'responsible_researcher' => ['required', 'string'],
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        $sanitized['data_type'] = json_encode($sanitized['data_type']);

        return $sanitized;
    }
}
